Tento repozitář obsahuje [userscript](https://wiki.greasespot.net/User_script),
který vylepšuje [mapu pokrytí CETINu](https://www.cetin.cz/domacnosti/overit-dostupnost/).

![Screenshot (vysoký zoom)](screenshot.png)

# Instalace

Userscripty se instalují do prohlížeče přes doplněk, jako třeba [Violentmonkey](https://violentmonkey.github.io/).
Stačí [soubor otevřít](https://gitlab.com/dvdkon/zrychlujemecesko-userscript/-/raw/main/bettermap.user.js)
a zvolit instalaci. Při příštím načtení [mapy rychlosti](https://www.cetin.cz/domacnosti/overit-dostupnost/)
bude původni mapa nahrazena vylepšenou.
