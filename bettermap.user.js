// ==UserScript==
// @name        CETIN Better Map
// @namespace   Violentmonkey Scripts
// @match       https://www.cetin.cz/domacnosti/overit-dostupnost/
// @version     2.0
// @author      David Koňařík <dvdkon@konarici.cz>
// @description Replaces the map with a recolored Leaflet one, exposing all provided information
// @require     https://unpkg.com/leaflet@1.7.1/dist/leaflet.js
// ==/UserScript==

const alpha = 255;
const speedToColor = {
  2: [50, 0, 0, alpha],
  4: [60, 0, 0, alpha],
  8: [80, 0, 0, alpha],
  10: [100, 0, 0, alpha],
  16: [150, 0, 0, alpha],
  20: [177, 0, 0, alpha],
  40: [230, 40, 0, alpha],
  50: [255, 63, 0, alpha],
  100: [255, 191, 0, alpha],
  160: [231, 219, 0, alpha],
  180: [222, 229, 0, alpha],
  200: [213, 239, 0, alpha],
  210: [210, 243, 0, alpha],
  250: [200, 255, 0, alpha],
  1000: [30, 230, 230, alpha],
  2000: [5, 252, 252, alpha],
};

const speedToIcon = {};
let speedIconsCss = "";
for(const [speed, color] of Object.entries(speedToColor)) {
  speedToIcon[speed] = L.divIcon({className: "bettermap-icon-speed-" + speed});
  speedIconsCss += `
.bettermap-icon-speed-${speed} {
  background-color: rgb(${color[0]}, ${color[1]}, ${color[2]});
  border-radius: 5px;
}`;
}
speedIconsCss += `
.bettermap-icon-speed-null {
  background-color: #AAAAAA;
  border-radius: 5px;
  color: #FFF;
}

.bettermap-icon-speed-other {
  background-color: rebeccapurple;
  border-radius: 5px;
}`;
const speedIconNull = L.divIcon({className: "bettermap-icon-speed-null"});
const speedIconOther = L.divIcon({className: "bettermap-icon-speed-other"});

function loadBetterMap() {
  console.info("Loading CETIN Better Map userscript...");

  // Remove page content
  document.querySelector("#senna_surface1-default").remove();

  const createApi =
    Object.values(window).filter(x => {
      try {
        return typeof x === "function"
            && x.toString().includes("getSpeedsInArea");
      } catch { }
    });
  if(createApi.length === 0) {
    console.error("Did not find createApi method!");
  }
  const {getSpeedsInArea} = createApi[0]();

  const newStyle = document.createElement("style");
  newStyle.innerHTML = `
  body {
    margin: 0;
  }

  .page__content {
    padding: 0;
  }

  .vue-app {
    display: none;
  }

  #bettermap {
    height: 100vh;
    width: 100%;
  }

  .bettermap-marker-control {
    background-color: #FFF;
    padding: 3px;
    display: flex;
    flex-direction: row;
    gap: 10px;
    align-items: center;
  }

  .bettermap-marker-control button {
    background-color: #FFF;
    border-style: solid;
    border-width: 1px;
    border-radius: 2px;
    border-color: #AAA;
  }
  ` + speedIconsCss;
  document.querySelector("head").appendChild(newStyle);

  const leafletStyle = document.createElement("link");
  leafletStyle.rel = "stylesheet";
  leafletStyle.href = "https://unpkg.com/leaflet@1.7.1/dist/leaflet.css";
  document.querySelector("head").appendChild(leafletStyle);

  const mapElem = document.createElement("div");
  mapElem.id = "bettermap";
  document.querySelector("body").appendChild(mapElem);

  const map = L.map("bettermap").setView([50, 15], 8);
  window.bettermap = map;
  L.tileLayer("https://tiles.stadiamaps.com/tiles/stamen_toner/{z}/{x}/{y}{r}.png", {
    attribution: `
      <a href="www.openstreetmap.org">© OpenStreetMap contributors</a>
      (<a href="www.openstreetmap.org/copyright">terms</a>),
      <a href="https://stadiamaps.com/">Stadia Maps</a>,
      <a href="https://www.stamen.com/" target="_blank">Stamen Design</a>
    `,
    tileSize: 256,
  }).addTo(map);

  function attrToLine(prefix, value) {
    const c = document.createElement("div");
    if(value && typeof value == "object") {
      Object.entries(value)
        .map(kv => attrToLine(prefix + (prefix == "" ? "" : ".") + kv[0], kv[1]))
        .forEach(e => c.appendChild(e));
    } else {
      const k = document.createElement("b");
      k.innerText = prefix;
      const v = document.createElement("span");
      v.innerText = ": " + value;
      c.appendChild(k);
      c.appendChild(v);
    }
    return c;
  }

  let markerLayer;

  function queryMarkers() {
    // Going through the original app should keep Recaptcha running
    const bounds = map.getBounds();
    getSpeedsInArea(
      bounds.getNorth(),
      bounds.getWest(),
      bounds.getSouth(),
      bounds.getEast()
    ).then(resp => {
      if(resp.status !== 200) {
        alert("Error fetching markers: " + resp.status);
        return;
      }

      const markers = resp.data.data.map(feature => {
        let icon;
        if(!feature.current) {
          icon = speedIconNull;
        } else if(feature.current.downloadSpeed in speedToIcon) {
          icon = speedToIcon[feature.current.downloadSpeed];
        } else {
          icon = speedIconOther;
          console.warn("Unhandled speed", feature.current.downloadSpeed);
        }

        return L.marker([feature.location.lat, feature.location.lon], {
          icon,
        }).bindPopup(_ => attrToLine("", feature));
      });

      if(markerLayer) markerLayer.clearLayers();
      markerLayer = L.featureGroup(markers).addTo(map);
    });
  }

  MarkerControl = L.Control.extend({
    onAdd(map) {
      const wrapper = L.DomUtil.create("div", "bettermap-marker-control leaflet-bar");

      const getButton = L.DomUtil.create("button", "", wrapper);
      getButton.innerText = "Get";
      getButton.onclick = queryMarkers;

      const clearButton = L.DomUtil.create("button", "", wrapper);
      clearButton.innerText = "Clear";
      clearButton.onclick = () => { if (markerLayer) markerLayer.clearLayers(); };

      return wrapper;
    }
  });

  new MarkerControl({}).addTo(map);
}

const baseLoadInterval = setInterval(() => {
  if(document.querySelector("#app")) {
    clearInterval(baseLoadInterval);
    loadBetterMap();
    return;
  }
  console.log("Did not find #app, retrying...");
}, 100);
